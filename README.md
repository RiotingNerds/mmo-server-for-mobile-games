# README #

This is a learning development of simple turn base erlang game server. According to theory, erlang will be able to take 50% of Wooga populate game user base with just 1 server.

- [Source #1](http://smyck.net/2012/04/22/why-erlang/)
- [Source #2](http://www.slideshare.net/wooga/erlang-the-big-switch-in-social-games)

Some learning sites for referrence

- [Source #3](http://www.wooga.com/2013/02/erlang-factory/)
- [Source #4](http://learnyousomeerlang.com/)

Will be updating the development process in [Rioting Nerds](http://riotingnerds.com)

To Start

1. In navigate to /src folder
2. go into erl shell
3. type `c(main).`
4. type `main:start().`


To test with tcp client download [tcptester](http://sockettest.sourceforge.net/)

1. enter port 2345
2. and hit connect.

# Version 0.1 #
When connected, hit `aaa` and server will return bbb. This version is to add simple data manipulation and return to client server.