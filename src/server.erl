%%%-------------------------------------------------------------------
%%% @author chrissim
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 11. Aug 2014 10:54 PM
%%%-------------------------------------------------------------------
-module(server).
-author("chrissim").
-define(PORTNO, 1234).

%% API
-export([]).

init([]) ->
  case gen_tcp:listen(?PORTNO, [{active, false}, binary, {packet, 2}]) of
    {ok, Socket} -> {ok, SupervisorFlags, [ChildDefinition]};
    {error, Error} -> {error, Error}
  end.
