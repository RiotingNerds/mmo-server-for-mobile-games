%%%-------------------------------------------------------------------
%%% @author Chris Sim
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 11. Aug 2014 6:02 PM
%%%-------------------------------------------------------------------
-module(main).
-author("Chris Sim").
-define(PORTNO, 2345).
-export([start/0, server/3, loop/0, check_recv/4]).
-record(room,{clients=[]}).
-record(client,{socket}).

start() ->
  case gen_tcp:listen(?PORTNO, [binary, {packet, 0},
    {active, false}]) of
    {ok, ListenSock} ->
      start_servers(1,ListenSock),
      {ok, _} = inet:port(ListenSock);
    {error,Reason} ->
      {error,Reason}
  end.

start_servers(0,_) ->
  ok;
start_servers(Num,LS) ->
  Res = spawn(?MODULE, loop, []),
  spawn(?MODULE, server, [LS,Res,#room{clients = []}]),
  start_servers(Num-1,LS).

server(LS,Res,Room) ->
  case gen_tcp:accept(LS) of
    {ok,S} ->
      gen_tcp:send(S,"Welcome guest."),
      OldClientList = Room#room.clients,
      NewClientList  = [OldClientList|#client{socket = S}],
      NewRoom = Room#room{clients = NewClientList},
      spawn(?MODULE, check_recv, [S,[],Res,NewRoom]),
      %%check_recv(S,[],Res),
      %%spawn(?MODULE, check_recv, [S,[],Res]),
      server(LS,Res,NewRoom);
    Other ->
      io:format("accept returned ~w - goodbye!~n",[Other]),
      ok
  end.

check_recv(S, Bs,Res,Room) ->
  case gen_tcp:recv(S, 0) of
    {ok, B} ->
      Res ! {tcp, S, B},
      check_recv(S, [Bs, B],Res,Room);
    {error, closed} ->
      io:format("Inside Res: ~w~n",[S]),
      {ok, list_to_binary(Bs)}
  end.

loop() ->
  %%inet:setopts(S,[{active,once}]),
  receive
    {tcp,S,Data,Room} ->
      io:format("Getting Data ~w~n",[Room#room.clients]),
      Answer = process(Data), % Not implemented in this example
      io:format("Processed Data ~w",[Answer]),
      gen_tcp:send(S,string:concat("Server: ",Answer)),
      loop();
    {tcp_closed,S} ->
      io:format("Socket ~w closed [~w]~n",[S,self()]),
      ok;
    Other ->
      io:format("accept returned ~w - goodby111e!~n",[Other])
  end.


process(Data) ->
  case binary_to_list(Data) of
    "aaa\r\n" ->
      "bbb";
    _ -> "unknown command"
  end.
%% API
