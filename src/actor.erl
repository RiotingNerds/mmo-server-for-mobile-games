%%%-------------------------------------------------------------------
%%% @author Chris Sim
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 13. Aug 2014 5:15 PM
%%%-------------------------------------------------------------------
-module(actor).
-author("Chris Sim").

%% API
-export([run/0,counter/1,send_msgs/2]).

run() ->
  S = spawn(actor, counter, [0]),
  send_msgs(S, 100000).
counter(Sum) ->
  receive
    value -> io:fwrite("Value is ~w~n", [Sum]);
    {inc, 0} -> io:format("ended value is ~w~n",[0]);
    {inc, Amount} -> io:format("ended value is ~w~n",[Amount]),
                      counter(Amount)
    %%{inc, Amount} -> counter(Sum+Amount)
  end.
send_msgs(S, 0) ->
  S ! {inc, 0},
  true;
send_msgs(S, Count) ->
  S ! {inc, Count},
  send_msgs(S, Count-1).

% Usage:
%    1> c(counter).
%    2> S = counter:run().
%       ... Wait a bit until all children have run ...
%    3> S ! value.
%    Value is 100000