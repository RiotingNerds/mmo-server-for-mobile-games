%%%-------------------------------------------------------------------
%%% @author Chris Sim
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 16. Aug 2014 12:55 AM
%%%-------------------------------------------------------------------
-module(testlist).
-author("Chris Sim").

%% API
-export([empty_state/0, some_state/0,
  add_client/1, del_client/1,
  get_clients/1]).

-record(state,
{
  clients = []   ::[pos_integer()],
  dbname         ::char()
}).

empty_state() ->
  #state{}.

some_state() ->
  #state{
    clients = [],
    dbname  = "QA"}.

del_client(Client) ->
  S = some_state(),
  C = S#state.clients,
  S#state{clients = lists:delete(Client, C)}.

add_client(Client) ->
  S = some_state(),
  C = S#state.clients,
  S#state{clients = [Client|C]}.

get_clients(#state{clients = C, dbname = _D}) ->
  C.